// LosowanieKostek.cpp : Defines the entry point for the console application.
//
#include "stdafx.h"
#include <iostream>
#include <time.h>
#include <conio.h>
#include <cmath>
#include <string>
#include <Windows.h>
#include <omp.h>
#define M_PI 3.14159265358979323846

using namespace std;

void czekaj(int ms) {
	clock_t time_end;
	time_end = clock() + ms*CLOCKS_PER_SEC / 1000;	
	while (clock() < time_end) 
	{

	}
		
}

int main()
{
	
	const int n = 6000;
	double t[4];
	int los1, kostki[n];
	int rob = 0;
	int n1 = 0, n2 = 0, n3 = 0, n4 = 0, n5 = 0, n6 = 0;
	int m1 = 0, m2 = 0, m3 = 0, m4 = 0, m5 = 0, m6 = 0;
	long long entro = 0;
	long long s;
	long long s1;
	long long s2;
	long long s3;
	long long s4;
	long long s5;
	long long s6;
	int a;
	int clock1;
	int clock2;
	int clock3;

	omp_set_num_threads(4);
	clock1=clock();
#pragma omp parallel default(none) shared(kostki)
#pragma omp for
	for (int i = 0; i <n; i++) 
	{
		//czekaj(1);
		kostki[i] = 6;
	}
#pragma omp parallel default(none) private(n1,n2,n3,n4,n5,n6)
#pragma omp for
	for (int kk = 0; kk < 1000; kk++) {
		
#pragma omp parallel default(none) private(los1) shared(kostki)
#pragma omp for
		for (int ii = 0; ii < n; ii++)
 {
			los1 = rand() % 20;
			
			if (los1 == 0) {
				kostki[ii] = rand() % 6 + 1;
			}
		}

#pragma omp parallel default(none) shared(kostki) //private(n1,n2,n3,n4,n5,n6)
#pragma omp for
		for (int jj = 0; jj < n; jj++) 
		{
			if (kostki[jj] == 1) {
				n1++;
			}
			else if (kostki[jj] == 2) {
				n2++;
			}
			else if (kostki[jj] == 3) {
				n3++;
			}
			else if (kostki[jj] == 4) {
				n4++;
			}
			else if (kostki[jj] == 5) {
				n5++;
			}
			else if (kostki[jj] == 6) {
				n6++;
			}
		}

		s = n*(log(n) - 1) + 0,5*log(2 * M_PI*n);
		s1 = n1*(log(n1) - 1) + 0.5 * log(2 * M_PI*n1);
		s2 = n2*(log(n2) - 1) + 0.5 * log(2 * M_PI*n2);
		s3 = n3*(log(n3) - 1) + 0.5 * log(2 * M_PI*n3);
		s4 = n4*(log(n4) - 1) + 0.5 * log(2 * M_PI*n4);
		s5 = n5*(log(n5) - 1) + 0.5 * log(2 * M_PI*n5);
		s6 = n6*(log(n6) - 1) + 0.5 * log(2 * M_PI*n6);
		
		entro = s - (s1 + s2 + s3 + s4 + s5 + s6);


		cout << "J: " << n1 << " D: " << n2 << " T: " << n3 << " C: " << n4 << " P: " << n5 << " S: " << n6 << " E: " << entro << "\n";


		m1 = n1; m2 = n2; m3 = n3; m4 = n4; m5 = n5; m6 = n6;
		n1 = 0; n2 = 0; n3 = 0; n4 = 0; n5 = 0; n6 = 0; rob = 0;
	
	}
	
	clock2 = clock();
	clock3 = clock2 - clock1;
	cout << "Czas trwania programu: " << clock3;

	_getch();
    return 0;
}